-- Var def
local sleep_value = 1
local output_channel = 12345
local reply_channel = 54321

local coolant_threshold = 75
local fuel_threshold = 25
local heated_coolant_threshold = 90
local waste_threshold = 50
local damage_threshold = 5

local reactor_values =
{
	coolant = -1,
	fuel = -1,
	heated_coolant = -1,
	waste = -1,
	damage = -1,
	fuel_consumption = -1
}
local reactor_status =
{
	coolant_level = "na",
	fuel_level = "na",
	heated_coolant_level = "na",
	waste_level = "na",
	damage_level = "na",
	status = "na"
}

-- IO init
local reactor = peripheral.find("fissionReactorLogicAdapter") or error("No reactor logic adapter attached", 0)
local modem = peripheral.find("modem") or error("No modem attached", 0)
modem.open(reply_channel)

-- Update reactor values function
local function update_values()

	reactor_values.coolant = reactor.getCoolantFilledPercentage() * 100
	reactor_values.fuel = reactor.getFuelFilledPercentage() * 100
	reactor_values.heated_coolant = reactor.getHeatedCoolantFilledPercentage() * 100
	reactor_values.waste = reactor.getWasteFilledPercentage() * 100
	reactor_values.damage = reactor.getDamagePercent() * 100
	-- TODO: add fuel consumption

end

-- Update reactore status function
local function update_status()

	reactor_status.coolant_level = reactor_values.coolant > coolant_threshold
	reactor_status.fuel_level = reactor_values.fuel > fuel_threshold
	reactor_status.heated_coolant_level = reactor_values.heated_coolant < heated_coolant_threshold
	reactor_status.waste_level = reactor_values.waste < waste_threshold
	reactor_status.damage_level = reactor_values.damage < damage_threshold
	reactor_status.status = reactor.getStatus()

end

-- Control function
local function control()

	if(reactor_status.status)
	then
		if(not(reactor_status.coolant_level) or not(reactor_status.fuel_level) or not(reactor_status.waste_level) or not(reactor_status.heated_coolant_level) or not(reactor_status.damage_level))
		then
			reactor.scram()
		end
	else
		if(reactor_status.coolant_level and reactor_status.fuel_level and reactor_status.waste_level and reactor_status.heated_coolant_level and reactor_status.damage_level)
		then
			reactor.activate()
		end
	end

end

-- Send value function

-- Send status function
local function send_values(m)
	m.transmit(output_channel, reply_channel, reactor_values)
end

local function send_status(m)
	m.transmit(output_channel, reply_channel, reactor_status)
end

-- Main code
print("Fission control started")
while(true)
do
	update_values(modem)
	update_status(modem)
	control()
	os.sleep(sleep_value)
end

